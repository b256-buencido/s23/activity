console.log("Hello");

// Initialize/add the following trainer object properties:
// Name (String)
// Age (Number)
// Pokemon (Array)
// Friends (Object with Array values for properties)
let myProfile = {

	name: "Ash Ketchum",
	age: " 10",
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur",],
	friends: {
		kanto: ["Brock", "Misty"],
		hoenn: ["May", "Max"],
	},
	// Initialize/add the trainer object method named talk that prints out the message: Pikachu! I choose you!
	talk: function() {
		console.log("Pikachu! I choose you!")
	}

}
console.log(myProfile);

// Access the trainer object properties using dot and square bracket notation.
console.log("Result of dot notation");

console.log(myProfile.name);

console.log("Result of square bracket notation");
console.log(myProfile["pokemon"]);

// Invoke/call the trainer talk object method.
myProfile.talk();


// Create a constructor for creating a pokemon with the following properties:
// Name (Provided as an argument to the constructor)
// Level (Provided as an argument to the constructor)
// Health (Create an equation that uses the level property)
// Attack (Create an equation that uses the level property)
function myPokemon(name, level) {
	this.name = name;
	this.level = level;
	this.health = level * 2;
	this.attack = level;


// Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
	this.tackle = function(targetPokemon) {
		targetPokemon.health -= this.attack;
		console.log(this.name + " tackled " + targetPokemon.name);
		console.log(targetPokemon.name + " health is now reduced to " + targetPokemon.health);

    // Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
		if (targetPokemon.health <= 0) {
			this.faint(targetPokemon);
			
		}

	};

  // Create a faint method that will print out a message: targetPokemon has fainted.
	this.faint = function(targetPokemon) {
		console.log(targetPokemon.name + " fainted.");
	};

		

}

// Create/instantiate several pokemon objects from the constructor with varying name and level properties.
let pikachu =  new myPokemon("Pikachu", 12);
console.log(pikachu);

let geodude =  new myPokemon("Geodude", 8);
console.log(geodude);

let mewtwo =  new myPokemon("Mewtwo", 100);
console.log(mewtwo);


	// this.tackle: function(targetPokemon){
	// 	targetPokemon.health -= this.attack;
	// 	console.log(this.name + " tackled " + targetPokemon);

	// }

	// this.faint: function(targetPokemon) {
	// 	console.log(targetPokemon.name + " health is now reduced to " + targetPokemon.health - this.attack);
	// 	console.log(targetPokemon.name + "fainted");
	// }

// Invoke the tackle method of one pokemon object to see if it works as intended.
mewtwo.tackle(geodude);
// console.log(geodude.health);

console.log(geodude);



